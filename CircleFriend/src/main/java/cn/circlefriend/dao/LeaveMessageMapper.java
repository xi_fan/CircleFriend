package cn.circlefriend.dao;

import java.util.ArrayList;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import cn.circlefriend.pojo.LeaveMessage;
@Repository
public interface LeaveMessageMapper {

    int deleteByPrimaryKey(String leavemessageId);

    int insert(LeaveMessage record);

    int insertSelective(LeaveMessage record);

    ArrayList<LeaveMessage> selectByPrimaryKey(LeaveMessage leaveMessage);

    int updateByPrimaryKeySelective(LeaveMessage record);

    int updateByPrimaryKey(LeaveMessage record);
}