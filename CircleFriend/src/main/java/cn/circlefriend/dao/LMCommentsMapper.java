package cn.circlefriend.dao;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import cn.circlefriend.pojo.LMComments;
@Repository
public interface LMCommentsMapper {
    int deleteByPrimaryKey(String lmcommentsId);

    int insert(LMComments record);

    int insertSelective(LMComments record);

    LMComments selectByPrimaryKey(String lmcommentsId);

    int updateByPrimaryKeySelective(LMComments record);

    int updateByPrimaryKeyWithBLOBs(LMComments record);

    int updateByPrimaryKey(LMComments record);
}