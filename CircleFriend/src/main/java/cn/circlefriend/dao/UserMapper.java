package cn.circlefriend.dao;

import java.util.ArrayList;

import org.springframework.stereotype.Repository;

import cn.circlefriend.pojo.User;
@Repository
public interface UserMapper {
    int deleteByPrimaryKey(String userId);

    String insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(User record);
    
    ArrayList<User> selectByPrimaryKeyToList(String album_aid);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
    
}