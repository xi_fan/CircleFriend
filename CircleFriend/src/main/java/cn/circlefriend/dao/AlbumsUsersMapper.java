package cn.circlefriend.dao;

import org.apache.ibatis.annotations.Param;

import cn.circlefriend.pojo.AlbumsUsers;

public interface AlbumsUsersMapper {
	int deleteByPrimaryKey(String albumsusersId);

	int deleteByTwoKeys(@Param("albumsusersAid") String albumsusersAid,
			@Param("albumsusersUid")String albumsusersUid);

	int insert(AlbumsUsers record);

	int insertSelective(AlbumsUsers record);

	AlbumsUsers selectAlbumsUsers(AlbumsUsers record);

	int updateByPrimaryKeySelective(AlbumsUsers record);

	int updateByPrimaryKey(AlbumsUsers record);
	
}