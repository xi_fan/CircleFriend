package cn.circlefriend.dao;

import java.util.ArrayList;

import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;

import cn.circlefriend.pojo.Picture;

@Repository
public interface PictureMapper {
	int deleteByPrimaryKey(String pictureId);

	String insert(Picture record);

	int insertSelective(Picture record);

	ArrayList<Picture> selectByPrimaryKey(RowBounds rowBounds, Picture record);

	int updateByPrimaryKeySelective(Picture record);

	int updateByPrimaryKey(Picture record);

	int getAllRows(Picture record);
}