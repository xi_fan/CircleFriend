package cn.circlefriend.dao;

import org.springframework.stereotype.Repository;

import cn.circlefriend.pojo.Weather;
@Repository
public interface WeatherMapper {
    int deleteByPrimaryKey(String weatherId);

    int insert(Weather record);

    int insertSelective(Weather record);

    Weather selectByPrimaryKey(String weatherUid);

    int updateByPrimaryKeySelective(Weather record);

    int updateByPrimaryKey(Weather record);
}