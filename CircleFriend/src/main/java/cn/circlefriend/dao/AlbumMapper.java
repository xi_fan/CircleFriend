package cn.circlefriend.dao;

import java.util.ArrayList;

import org.springframework.stereotype.Repository;

import cn.circlefriend.pojo.Album;
@Repository
public interface AlbumMapper {
    int deleteByPrimaryKey(String albumId);

    int insert(Album record);

    int insertSelective(Album record);

    Album selectByPrimaryKey(String albumId);

    int updateByPrimaryKeySelective(Album record);

    int updateByPrimaryKey(Album record);
    
    ArrayList<Album> getAlbumsByUserId(String userId);
    
    Album selectAlbumId(Album record);
}