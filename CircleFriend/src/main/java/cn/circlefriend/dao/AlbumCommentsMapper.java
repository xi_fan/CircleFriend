package cn.circlefriend.dao;

import java.util.ArrayList;

import org.springframework.stereotype.Repository;

import cn.circlefriend.pojo.AlbumComments;
@Repository
public interface AlbumCommentsMapper {
    int insert(AlbumComments record);
    ArrayList<AlbumComments> getAllCommentsByPid(String albumcommentsBelongaid);
    int insertSelective(AlbumComments record);
}