/**
 * @author:稀饭
 * @time:下午3:04:39
 * @filename:FileOperationUtil.java
 */
package cn.circlefriend.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;

import org.junit.Test;

public class FileOperationUtil {

	/**
	 * @描述：向指定的文件添加内容
	 * @时间：下午3:09:11
	 * @开发者：稀饭 @测试：
	 * @param filePath
	 *            路徑 htmlName 文明名（包含类型）content 指定內容
	 */
	public static void appendFile(String filePath, String htmlName, String content) {
		RandomAccessFile rf = null;
		try {
			rf = new RandomAccessFile(filePath + htmlName, "rw");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			rf.seek(rf.length());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // 将指针移动到文件末尾
		try {
			rf.writeBytes(content);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			rf.close(); // 关闭文件流
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @描述：创建文件
	 * @时间：下午3:10:06
	 * @开发者：稀饭
	 * @测试：通过
	 * @param 路径
	 *            文件名 文件内容
	 */
	public static void createHtml(String filePath, String htmlName, String contentData) {
		File file = new File(filePath + htmlName);
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		try {
			fileWriter.write(contentData);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		try {
			if (fileWriter != null)
				fileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @描述：创建多个文件夹
	 * 
	 * @时间：下午7:09:11
	 * @开发者：稀饭 @测试：通过
	 * @param
	 */
	public static void createDir(String path) {
		if (path.contains("\\.")) {
			path = path.substring(0, path.lastIndexOf("\\"));
		}
		File pathFile = new File(path);
		if (!pathFile.exists()) {
			pathFile.mkdirs();
		}
	}

	@Test
	public void readLineContent() {
		FileReader file = null;
		try {
			file = new FileReader("pinfo/4.txt");
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String result = "";
		String line = "";
		BufferedReader br = new BufferedReader(file);
		try {
			int i = 0;
			System.out.println("if (file1.equals(\"/tmp/ckFile4.csv\")) {");
			while ((line = br.readLine()) != null) {
				result += line + "\\n";
				i++;
				if (i % 10 == 0) {
					System.out.println("System.out.print(\"" + result + "\");");
					result = "";
				}
			}
			System.out.println("System.out.print(\"" + result + "\");");
			System.out.println("}");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
