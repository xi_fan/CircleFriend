package cn.circlefriend.controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import cn.circlefriend.pojo.User;


/**
 * 主要实现一些公共的函数
 */

@Controller
public class BaseController {

	@Autowired
	private HttpSession session;
	@Autowired
	private ServletContext context;

	/**
	 * 用户信息加入session
	 * 
	 * @param userInfo
	 */
	public void setUserInfo(User user) {
		if (null != user) {
			session.setAttribute("userInfo", user);
			session.setMaxInactiveInterval(1800);// 设置session超时时间
		}
	}

	/**
	 * 从session中获取用户信息
	 * 
	 * @return
	 */
	public User getUserInfo() {
		User userInfo = (User) session.getAttribute("userInfo");
		return (userInfo != null ? userInfo : null);
	}

	public ServletContext getContext() {
		return context;
	}

	/**
	 * 清除session
	 * 
	 */
	public void clearSession() {
		//session.removeAttribute("userInfo");
		session.invalidate();
	}

}
