/**
 * @author:稀饭
 * @time:上午9:57:37
 * @filename:UserController.java
 */
package cn.circlefriend.controller;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.circlefriend.pojo.User;
import cn.circlefriend.service.UserService;
import cn.circlefriend.util.ConnectionUtil;
import cn.circlefriend.util.Data;

import com.alibaba.fastjson.JSONObject;

@Controller
@RequestMapping("/userController")
public class UserController extends BaseController {

	@Autowired
	private UserService service;

	/**
	 * @描述：
	 * @时间：下午9:22:00
	 * @开发者：稀饭
	 * @测试：修改密码
	 */
	@ResponseBody
	@RequestMapping("/updatePassword")
	public Map<String, Object> updatePassword(User record) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			service.updateByPrimaryKeySelective(record);
			map.put("result", "success");
		} catch (Exception e) {
			// TODO: handle exception
			map.put("result", "failure");
		}
		return map;
	}

	/**
	 * @描述：
	 * @时间：下午9:25:06
	 * @开发者：稀饭
	 * @测试：通過
	 */
	@RequestMapping("/updateUserTruename")
	@ResponseBody
	public Map<String, Object> updateUserTruename(User record) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			service.updateByPrimaryKeySelective(record);
			map.put("result", "success");
		} catch (Exception e) {
			// TODO: handle exception
			map.put("result", "failure");
		}
		return map;
	}

	/**
	 * @描述：
	 * @时间：下午9:25:20
	 * @开发者：稀饭
	 * @测试：通過
	 */
	@RequestMapping("/updateWechatName")
	public Map<String, Object> updateWechatName(User record) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			service.updateByPrimaryKeySelective(record);
			map.put("result", "success");
		} catch (Exception e) {
			// TODO: handle exception
			map.put("result", "failure");
		}
		return map;
	}
	
	/**
	 * @描述：
	 * @时间：上午12:26:41
	 * @开发者：稀饭
	 * @测试：
	 */
	@RequestMapping("/createUser")
	@ResponseBody
	public Map<String, Object> createUser(String code,User user) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			user.setUserWechatname(URLDecoder.decode(user.getUserWechatname().toString(),"UTF-8"));
			String url = Data.openIdUrl.replace("CODE", code).replace("AppSecret", Data.AppSecret).replace("AppID", Data.AppID);
			String result = ConnectionUtil.sendGet(url, null);
			JSONObject object = JSONObject.parseObject(result.replace("null", ""));
			user.setUserPassword(object.getString("openid"));
			User user2 = service.selectByPrimaryKey(user);
			String userId;
			if(user2 != null)
			{
				userId = user2.getUserId();
			}
			else
			{
				userId = service.insert(user);
			}
			map.put("result", userId);
			map.put("status", "200");
		} catch (Exception e) {
			map.put("status", "300");
		}
		return map;
	}
}
