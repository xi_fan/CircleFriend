/**
 * @author:稀饭
 * @time:上午9:58:21
 * @filename:AlbumCommentsController.java
 */
package cn.circlefriend.controller;

import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.circlefriend.pojo.AlbumComments;
import cn.circlefriend.service.AlbumCommentsService;

@Controller
@RequestMapping("/albumCommentsController")
public class AlbumCommentsController extends BaseController {
	@Autowired
	AlbumCommentsService albumCommentsService;

	@ResponseBody
	@RequestMapping("/addComments")
	public Map<String, Object> addComments(AlbumComments albumComments) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			albumComments.setAlbumcommentsContents(URLDecoder.decode(albumComments.getAlbumcommentsContents().toString(),"UTF-8"));
			String createTime = (new SimpleDateFormat("yyyy-MM-dd HH:mm"))
				.format(new Date());
			albumComments.setAlbumcommentsTime(createTime);
			int i = albumCommentsService.insert(albumComments);
			if (i != 1) {
				map.put("result", "success");
			} else {
				map.put("result", "failure");
			}
		} catch (Exception e) {
			// TODO: handle exception
			map.put("result", "error");
		}
		return map;
	}
	
	@ResponseBody
	@RequestMapping("/getAllCommentsByPid")
	public Map<String, Object> getAllCommentsByPid(String albumcommentsBelongaid) {
		
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			ArrayList<AlbumComments> comments = albumCommentsService.getAllCommentsByPid(albumcommentsBelongaid);
			map.put("result", comments);
			map.put("status", 200);
		} catch (Exception e) {
			// TODO: handle exception
			map.put("status", 300);
		}
		return map;
	}
	
	@ResponseBody
	@RequestMapping("/test")
	public Map<String, Object> test() {
		AlbumComments albumComments = new AlbumComments();
		albumComments
				.setAlbumcommentsBelongaid("7b08a487aec511e6a48a142d27fd7e9e");
		albumComments.setAlbumcommentsContents("哎呦不錯哦！");
		albumComments.setAlbumcommentsToid("33aef7e0ac1b11e6af9f142d27fd7e9e");
		albumComments.setAlbumcommentsWhoid("5d757b9ca2ab11e69a34142d27fd7e9e");
		return addComments(albumComments);
	}
}
