/**
 * @author:稀饭
 * @time:上午9:59:23
 * @filename:PictureController.java
 */
package cn.circlefriend.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import cn.circlefriend.common.PageInfo;
import cn.circlefriend.pojo.Picture;
import cn.circlefriend.service.PictureService;
import cn.circlefriend.util.Data;
import cn.circlefriend.util.FileOperationUtil;

@Controller
@RequestMapping("/pictureController")
public class PictureController extends BaseController {
	@Autowired
	private PictureService pictureService;

	@RequestMapping("/getPicturesByAid")
	@ResponseBody
	public Map<String, Object> getPicturesByAid(
			@ModelAttribute(value = "pageInfo") PageInfo<Picture> pageInfo,
			Picture record) {
		System.out.println(pageInfo.getCurrentPage());
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			ArrayList<Picture> list = pictureService.selectByPrimaryKey(
					pageInfo, record);
			map.put("status", 200);
			map.put("result", list);
		} catch (Exception e) {
			// TODO: handle exception
			map.put("status", 300);
		}
		return map;
	}

	/**
	 * @描述：插入相片信息以及上传相片
	 * @时间：上午10:43:53
	 * @开发者：稀饭
	 * @测试：通过
	 */
	@RequestMapping("/insertPic")
	@ResponseBody
	public Map<String, Object> insertPic(Picture record, MultipartFile fileData) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (fileData.getSize() != 0) {
			String fileName = fileData.getOriginalFilename();
			String pictureId = null;
			String createTime = (new SimpleDateFormat("yyyy-MM-dd HH:mm"))
					.format(new Date());
			String name = (new SimpleDateFormat("yyyyMMddHHmmssSSS"))
					.format(new Date()) + "." + fileName.split("\\.")[1];
			record.setPictureName(name);
			record.setPictureUploadtime(createTime);
			boolean check = false;
			try {
				pictureId = pictureService.insert(record);

			} catch (Exception e) {
				map.put("status", 300);
				// TODO: handle exception
			}
			String filePath = Data.filePath + record.getPictureAid() + "/"
					+ name;
			File file = new File(filePath);
			FileOperationUtil.createDir(filePath);
			try {
				fileData.transferTo(file);
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				map.put("status", 300);
				check = true;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				check = true;
			} finally {
				if (check == true)
					deletePic(pictureId);
			}

			map.put("status", 200);
		} else
			map.put("status", 199);
		return map;
	}

	/**
	 * @描述：删除指定相片
	 * @时间：上午10:47:10
	 * @开发者：稀饭
	 * @测试：通过
	 */
	public void deletePic(String pictureId) {
		try {
			pictureService.deleteByPrimaryKey(pictureId);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@RequestMapping("/test")
	@ResponseBody
	public Map<String, Object> test() {
		PageInfo<Picture> pageInfo = new PageInfo<>();
		pageInfo.setPageSize(10);
		pageInfo.setFromRecord(0);
		Picture picture = new Picture();
		picture.setPictureAid("93674f5ddec911e6ac82142d27fd7e9e");
		return  getPicturesByAid(pageInfo, picture);
	}
}
