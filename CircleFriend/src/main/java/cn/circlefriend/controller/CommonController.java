/**
 * @author:稀饭
 * @time:下午11:07:03
 * @filename:CommonController.java
 */
package cn.circlefriend.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.circlefriend.pojo.User;
import cn.circlefriend.service.UserService;

@Controller
@RequestMapping("/common")
public class CommonController extends BaseController {
	@Autowired
	public UserService userService;

	/**
	 * @描述：检查微信号是否已经注册登陆过，如果是则直接挑到主页面，如果不是则跳到登陆页面
	 * @时间：下午4:51:56
	 * @开发者：稀饭
	 * @测试：通过
	 */
	@RequestMapping("/existWechatName")
	public String existWechatName(User record) throws Exception {
		User user = userService.selectByPrimaryKey(record);
		if (user == null) {
			return "common/login";
		} else {
			super.setUserInfo(user);
			return "common/main";
		}
	}

	/**
	 * @描述：
	 * @时间：下午4:59:10
	 * @开发者：稀饭
	 * @测试：通过
	 */
	@RequestMapping("/exitSystem")
	public String exitSystem(User record) {
		try {
			super.clearSession();
			return "common/login";
		} catch (Exception e) {
			// TODO: handle exception
			return "error";
		}
	}

	/**
	 * @描述：注册
	 * @时间：下午5:02:17
	 * @开发者：稀饭
	 * @测试：通过
	 */
	@RequestMapping("/regist")
	public String regist(User record) {
		try {
			userService.insert(record);
			return "common/login";
		} catch (Exception e) {
			// TODO: handle exception
			return "error";
		}
	}

	/**
	 * @描述：
	 * @时间：下午8:46:41
	 * @开发者：稀饭
	 * @测试：通过
	 */
	@RequestMapping("/login")
	@ResponseBody
	public Map<String, Object> login(User record) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			User user = userService.selectByPrimaryKey(record);
			if (user == null) {
				map.put("status", 199);
			} else {
				map.put("status", 200);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			map.put("status", 500);
		}
		return map;
	}
}
