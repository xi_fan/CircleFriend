/**
 * @author:稀饭
 * @time:上午9:57:57
 * @filename:AlbumController.java
 */
package cn.circlefriend.controller;

import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.circlefriend.pojo.Album;
import cn.circlefriend.pojo.AlbumsUsers;
import cn.circlefriend.pojo.User;
import cn.circlefriend.service.AlbumService;
import cn.circlefriend.service.AlbumsUsersService;
import cn.circlefriend.service.UserService;

@Controller
@RequestMapping("/albumController")
public class AlbumController extends BaseController {
	@Autowired
	AlbumService albumService;
	@Autowired
	UserService userService;
	@Autowired
	AlbumsUsersService albumsUsersService;

	/**
	 * @描述：通过用户id获取相册信息
	 * @时间：上午10:51:46
	 * @开发者：稀饭
	 * @测试：
	 */
	@RequestMapping("/getAlbumsByUserId")
	@ResponseBody
	public Map<String, Object> getAlbumsByUserId(String userId) {
		Map<String, Object> map = new HashMap<String, Object>();
		 try {
		ArrayList<Album> list = albumService.getAlbumsByUserId(userId);
		map.put("status", 200);
		map.put("result", list);
		 } catch (Exception e) {
		 // TODO: handle exception
		 map.put("status", "500");
		 }
		return map;
	}

	/**
	 * @描述：创建相册
	 * @时间：上午9:46:12
	 * @开发者：稀饭
	 * @测试：通过
	 */
	@RequestMapping("/createAlbum")
	@ResponseBody
	public Map<String, Object> createAlbum(Album record) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			if (albumService.selectAlbumId(record) == null) {
				String createTime = (new SimpleDateFormat("yyyy-MM-dd HH:mm"))
						.format(new Date());

				record.setAlbumCreattime(createTime);
				record.setAlbumDescription(URLDecoder.decode(record
						.getAlbumDescription().toString(), "UTF-8"));
				record.setAlbumName(URLDecoder.decode(record.getAlbumName()
						.toString(), "UTF-8"));
				record.setAlbumPassword(URLDecoder.decode(record
						.getAlbumPassword().toString(), "UTF-8"));
				if (albumService.insert(record) > 0) {
					map.put("status", 200);
				} else {
					map.put("status", 199);
				}
			} else {
				map.put("status", 201);
			}
		} catch (Exception e) {
			// TODO: handle exception
			map.put("status", 300);
		}
		return map;
	}

	/**
	 * @描述：刪除相冊
	 * @时间：上午9:49:07
	 * @开发者：稀饭
	 * @测试：通过
	 */
	@RequestMapping("/deleteAlbum")
	public void deleteAlbum(String albumId) {
		try {
			albumService.deleteByPrimaryKey(albumId);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * @描述：更新相册
	 * @时间：上午10:05:32
	 * @开发者：稀饭
	 * @测试：通过
	 */
	@RequestMapping("/updateAlbum")
	public void updateAlbum(Album record) {
		try {
			albumService.updateByPrimaryKeySelective(record);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * @描述：通过相册id查找用户列表
	 * @时间：上午12:23:18
	 * @开发者：稀饭
	 * @测试：通过
	 */
	@RequestMapping("/findJoiner")
	public ArrayList<User> findJoiner(String album_aid) {
		return userService.selectByPrimaryKeyToList(album_aid);
	}

	/**
	 * @描述：通过相册id和用户id删除参与者
	 * @时间：下午8:57:43
	 * @开发者：稀饭
	 * @测试：
	 */
	@RequestMapping("/removeUserInAblum")
	public void removeUserInAblum(String album_aid, String album_uid) {
		albumsUsersService.deleteByTwoKeys(album_aid, album_uid);
	}

	/**
	 * @描述：插入对象
	 * @时间：下午9:20:47
	 * @开发者：稀饭
	 * @测试：
	 */
	@ResponseBody
	@RequestMapping("/addJoiner")
	public Map<String, Object> addJoiner(AlbumsUsers record, Album album) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			album.setAlbumName(URLDecoder.decode(album.getAlbumName()
					.toString(), "UTF-8"));
			album.setAlbumPassword(URLDecoder.decode(album.getAlbumPassword()
					.toString(), "UTF-8"));
			Album selectAlbum = albumService.selectAlbumId(album);
			if (selectAlbum != null) {
				record.setAlbumsusersAid(selectAlbum.getAlbumId());
				if (albumsUsersService.selectAlbumsUsers(record) == null) {
					albumsUsersService.insertSelective(record);
					map.put("status", 200);
				} else {
					map.put("status", 201);
				}
			} else {
				map.put("status", 199);
			}
		} catch (Exception e) {
			// TODO: handle exception
			map.put("status", 300);
		}
		System.out.println(map.get("status"));
		return map;
	}

	@RequestMapping("/test")
	public void test() {
		AlbumsUsers albumsUsers = new AlbumsUsers();
		albumsUsers.setAlbumsusersAid("a6c1ac56aec511e6a48a142d27fd7e9e");
		albumsUsers.setAlbumsusersUid("5d757b9ca2ab11e69a34142d27fd7e9e");

	}
}
