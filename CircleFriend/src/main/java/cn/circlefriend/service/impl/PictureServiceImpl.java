/**
 * @author:稀饭
 * @time:下午10:49:47
 * @filename:PictureServiceImpl.java
 */
package cn.circlefriend.service.impl;

import java.util.ArrayList;

import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.circlefriend.common.PageInfo;
import cn.circlefriend.dao.PictureMapper;
import cn.circlefriend.pojo.Picture;
import cn.circlefriend.service.PictureService;
@Service
public class PictureServiceImpl implements PictureService {
	
	@Autowired
	private PictureMapper pictureMapper;
	/** @Title: deleteByPrimaryKey 
	 * @Description: TODO
	 * @param pictureUploadid
	 * @return  
	 */
	@Override
	public int deleteByPrimaryKey(String pictureId) {
		// TODO Auto-generated method stub
		return pictureMapper.deleteByPrimaryKey(pictureId);
	}

	/** @Title: insert 
	 * @Description: TODO
	 * @param record
	 * @return  
	 */
	@Override
	public String insert(Picture record) {
		// TODO Auto-generated method stub
		return pictureMapper.insert(record);
	}

	/** @Title: insertSelective 
	 * @Description: TODO
	 * @param record
	 * @return  
	 */
	@Override
	public int insertSelective(Picture record) {
		// TODO Auto-generated method stub
		return 0;
	}

	/** @Title: selectByPrimaryKey 
	 * @Description: TODO
	 * @param pictureUploadid
	 * @return  
	 */
	@Override
	public ArrayList<Picture> selectByPrimaryKey(PageInfo<Picture> pageInfo,Picture record) {
		// TODO Auto-generated method stub
		if (pageInfo != null) {
			int records = pictureMapper.getAllRows(record);
			pageInfo.setTotalRecords(records);
		}
		ArrayList<Picture> list = pictureMapper.selectByPrimaryKey(new RowBounds(
				pageInfo.getFromRecord(), pageInfo.getPageSize()),
				record);
		return list;
	}

	/** @Title: updateByPrimaryKeySelective 
	 * @Description: TODO
	 * @param record
	 * @return  
	 */
	@Override
	public int updateByPrimaryKeySelective(Picture record) {
		// TODO Auto-generated method stub
		return 0;
	}

	/** @Title: updateByPrimaryKey 
	 * @Description: TODO
	 * @param record
	 * @return  
	 */
	@Override
	public int updateByPrimaryKey(Picture record) {
		// TODO Auto-generated method stub
		return 0;
	}

}
