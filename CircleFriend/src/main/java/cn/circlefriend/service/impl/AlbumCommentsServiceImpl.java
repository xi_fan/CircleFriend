/**
 * @author:稀饭
 * @time:下午10:42:12
 * @filename:AlbumCommentsServiceImpl.java
 */
package cn.circlefriend.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.circlefriend.dao.AlbumCommentsMapper;
import cn.circlefriend.pojo.AlbumComments;
import cn.circlefriend.service.AlbumCommentsService;
@Service
public class AlbumCommentsServiceImpl implements AlbumCommentsService {
	@Autowired
	private AlbumCommentsMapper albumCommentsMapper;
	/** @Title: insert 
	 * @Description: TODO
	 * @param record
	 * @return  
	 */
	@Override
	public int insert(AlbumComments record) {
		// TODO Auto-generated method stub
		return albumCommentsMapper.insert(record);
	}

	/** @Title: insertSelective 
	 * @Description: TODO
	 * @param record
	 * @return  
	 */
	@Override
	public int insertSelective(AlbumComments record) {
		// TODO Auto-generated method stub
		return 0;
	}

	/** @Title: getAllCommentsByPid 
	* @Description: TODO
	* @param albumsusersBelongaid
	* @return  
	*/ 
	@Override
	public ArrayList<AlbumComments> getAllCommentsByPid(
			String albumcommentsBelongaid) {
		// TODO Auto-generated method stub
		return albumCommentsMapper.getAllCommentsByPid(albumcommentsBelongaid);
	}

}
