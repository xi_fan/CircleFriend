/**
 * @author:稀饭
 * @time:下午11:50:15
 * @filename:AlbumsUsersServiceImpl.java
 */
package cn.circlefriend.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.circlefriend.dao.AlbumsUsersMapper;
import cn.circlefriend.pojo.AlbumsUsers;
import cn.circlefriend.service.AlbumsUsersService;

@Service
public class AlbumsUsersServiceImpl implements AlbumsUsersService {

	@Autowired
	AlbumsUsersMapper albumsUsersMapper;

	/**
	 * @Title: deleteByPrimaryKey
	 * @Description: TODO
	 * @param albumsusersAid
	 * @param albumsusersUid
	 * @return
	 */
	@Override
	public int deleteByTwoKeys(String albumsusersAid, String albumsusersUid) {
		return albumsUsersMapper
				.deleteByTwoKeys(albumsusersAid, albumsusersUid);
	}

	/**
	 * @Title: insertSelective
	 * @Description: TODO
	 * @param record
	 * @return
	 */
	@Override
	public int insertSelective(AlbumsUsers record) {
		// TODO Auto-generated method stub
		return albumsUsersMapper.insertSelective(record);
	}

	/** @Title: selectAlbumsUsers 
	* @Description: TODO
	* @param albumsusersId
	* @return  
	*/ 
	@Override
	public AlbumsUsers selectAlbumsUsers(AlbumsUsers record) {
		// TODO Auto-generated method stub
		return albumsUsersMapper.selectAlbumsUsers(record);
	}

}
