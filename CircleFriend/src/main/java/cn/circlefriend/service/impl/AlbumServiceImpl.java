/**
 * @author:稀饭
 * @time:下午10:45:56
 * @filename:AlbumServiceImpl.java
 */
package cn.circlefriend.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.circlefriend.dao.AlbumMapper;
import cn.circlefriend.pojo.Album;
import cn.circlefriend.service.AlbumService;

@Service
public class AlbumServiceImpl implements AlbumService {

	@Autowired
	private AlbumMapper albumMapper;

	/**
	 * @Title: deleteByPrimaryKey
	 * @Description: TODO
	 * @param albumId
	 * @return
	 */
	@Override
	public int deleteByPrimaryKey(String albumId) {
		// TODO Auto-generated method stub
		return albumMapper.deleteByPrimaryKey(albumId);
	}

	/**
	 * @Title: insert
	 * @Description: TODO
	 * @param record
	 * @return
	 */
	@Override
	public int insert(Album record) {
		// TODO Auto-generated method stub
		return albumMapper.insert(record);
	}

	/**
	 * @Title: insertSelective
	 * @Description: TODO
	 * @param record
	 * @return
	 */
	@Override
	public int insertSelective(Album record) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @Title: selectByPrimaryKey
	 * @Description: TODO
	 * @param albumId
	 * @return
	 */
	@Override
	public Album selectByPrimaryKey(String albumId) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @Title: updateByPrimaryKeySelective
	 * @Description: TODO
	 * @param record
	 * @return
	 */
	@Override
	public int updateByPrimaryKeySelective(Album record) {
		// TODO Auto-generated method stub
		return albumMapper.updateByPrimaryKeySelective(record);
	}

	/**
	 * @Title: updateByPrimaryKey
	 * @Description: TODO
	 * @param record
	 * @return
	 */
	@Override
	public int updateByPrimaryKey(Album record) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @Title: getAlbumsByUserId
	 * @Description: TODO
	 * @param userId
	 * @return
	 */
	@Override
	public ArrayList<Album> getAlbumsByUserId(String userId) {
		// TODO Auto-generated method stub
		return albumMapper.getAlbumsByUserId(userId);
	}

	/**
	 * @Title: selectAlbumId
	 * @Description: TODO
	 * @param record
	 * @return
	 */
	@Override
	public Album selectAlbumId(Album record) {
		// TODO Auto-generated method stub
		return albumMapper.selectAlbumId(record);
	}

}
