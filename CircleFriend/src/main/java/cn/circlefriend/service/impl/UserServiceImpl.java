/**
 * @author:稀饭
 * @time:下午10:50:25
 * @filename:UserServiceImpl.java
 */
package cn.circlefriend.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.circlefriend.dao.UserMapper;
import cn.circlefriend.pojo.AlbumsUsers;
import cn.circlefriend.pojo.User;
import cn.circlefriend.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	// @Autowired
	// private DAO daoSupport;
	@Autowired
	private UserMapper dao;

	/**
	 * @Title: deleteByPrimaryKey
	 * @Description: TODO
	 * @param userId
	 * @return
	 */
	@Override
	public int deleteByPrimaryKey(String userId) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @Title: insert
	 * @Description: TODO
	 * @param record
	 * @return
	 */
	@Override
	public String insert(User record) {
		// TODO Auto-generated method stub
		return dao.insert(record);
	}

	/**
	 * @Title: insertSelective
	 * @Description: TODO
	 * @param record
	 * @return
	 */
	@Override
	public int insertSelective(User record) {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @Title: selectByPrimaryKey
	 * @Description: TODO
	 * @param record
	 * @return
	 */
	public User selectByPrimaryKey(User record) {
//		daoSupport.test();
		return dao.selectByPrimaryKey(record);
	}

	/**
	 * @Title: updateByPrimaryKeySelective
	 * @Description: TODO
	 * @param record
	 * @return
	 */
	@Override
	public int updateByPrimaryKeySelective(User record) {
		// TODO Auto-generated method stub
		return dao.updateByPrimaryKeySelective(record);
	}

	/**
	 * @Title: updateByPrimaryKey
	 * @Description: TODO
	 * @param record
	 * @return
	 */
	@Override
	public int updateByPrimaryKey(User record) {
		// TODO Auto-generated method stub
		return 0;
	}


	/** @Title: selectByPrimaryKeyToList 
	* @Description: TODO
	* @param album_aid
	* @return  
	*/ 
	@Override
	public ArrayList<User> selectByPrimaryKeyToList(String album_aid) {
		// TODO Auto-generated method stub
		return dao.selectByPrimaryKeyToList(album_aid);
	}

}
