/**
 * @author:稀饭
 * @time:下午10:38:41
 * @filename:AlbumCommentsService.java
 */
package cn.circlefriend.service;

import java.util.ArrayList;

import cn.circlefriend.pojo.AlbumComments;

public interface AlbumCommentsService {
	int insert(AlbumComments record);
	int insertSelective(AlbumComments record);
	ArrayList<AlbumComments> getAllCommentsByPid(String albumsusersBelongaid);
}
