/**
 * @author:稀饭
 * @time:下午10:50:43
 * @filename:WeatherService.java
 */
package cn.circlefriend.service;

import cn.circlefriend.pojo.Weather;

public interface WeatherService {
	int deleteByPrimaryKey(String weatherId);

    int insert(Weather record);

    int insertSelective(Weather record);

    Weather selectByPrimaryKey(String weatherUid);

    int updateByPrimaryKeySelective(Weather record);

    int updateByPrimaryKey(Weather record);
}
