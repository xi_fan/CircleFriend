/**
 * @author:稀饭
 * @time:下午10:46:54
 * @filename:LeaveMessageService.java
 */
package cn.circlefriend.service;

import java.util.ArrayList;

import cn.circlefriend.pojo.LeaveMessage;

public interface LeaveMessageService {
	int deleteByPrimaryKey(String leavemessageId);

    int insert(LeaveMessage record);

    int insertSelective(LeaveMessage record);

    ArrayList<LeaveMessage> selectByPrimaryKey(LeaveMessage leaveMessage);

    int updateByPrimaryKeySelective(LeaveMessage record);

    int updateByPrimaryKey(LeaveMessage record);
}
