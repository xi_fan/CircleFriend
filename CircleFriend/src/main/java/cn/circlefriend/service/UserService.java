/**
 * @author:稀饭
 * @time:下午10:50:03
 * @filename:UserService.java
 */
package cn.circlefriend.service;

import java.util.ArrayList;

import cn.circlefriend.pojo.User;

public interface UserService {
	int deleteByPrimaryKey(String userId);

	String insert(User record);

	int insertSelective(User record);
	
	ArrayList<User> selectByPrimaryKeyToList(String album_aid);

	User selectByPrimaryKey(User record);

	int updateByPrimaryKeySelective(User record);

	int updateByPrimaryKey(User record);
}
