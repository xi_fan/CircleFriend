/**
 * @author:稀饭
 * @time:下午11:49:21
 * @filename:AlbumsUsersService.java
 */
package cn.circlefriend.service;

import cn.circlefriend.pojo.AlbumsUsers;

public interface AlbumsUsersService {
	
	int deleteByTwoKeys(String albumsusersAid, String albumsusersUid);
	
	int insertSelective(AlbumsUsers record);
	
	AlbumsUsers selectAlbumsUsers(AlbumsUsers record);

}
