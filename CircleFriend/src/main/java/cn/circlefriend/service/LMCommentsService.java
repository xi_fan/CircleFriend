/**
 * @author:稀饭
 * @time:下午10:48:16
 * @filename:LMCommentsMapper.java
 */
package cn.circlefriend.service;

import cn.circlefriend.pojo.LMComments;

public interface LMCommentsService {
	int deleteByPrimaryKey(String lmcommentsId);

    int insert(LMComments record);

    int insertSelective(LMComments record);

    LMComments selectByPrimaryKey(String lmcommentsId);

    int updateByPrimaryKeySelective(LMComments record);

    int updateByPrimaryKeyWithBLOBs(LMComments record);

    int updateByPrimaryKey(LMComments record);
}
