/**
 * @author:稀饭
 * @time:下午10:49:25
 * @filename:PictureService.java
 */
package cn.circlefriend.service;

import java.util.ArrayList;

import cn.circlefriend.common.PageInfo;
import cn.circlefriend.pojo.Picture;

public interface PictureService {
	int deleteByPrimaryKey(String pictureUploadid);

    String insert(Picture record);

    int insertSelective(Picture record);

    ArrayList<Picture> selectByPrimaryKey(PageInfo<Picture> pageInfo,Picture record);

    int updateByPrimaryKeySelective(Picture record);

    int updateByPrimaryKey(Picture record);
}
