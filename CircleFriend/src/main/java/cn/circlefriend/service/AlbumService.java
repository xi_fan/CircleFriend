/**
 * @author:稀饭
 * @time:下午10:45:01
 * @filename:AlbumMapper.java
 */
package cn.circlefriend.service;

import java.util.ArrayList;

import cn.circlefriend.pojo.Album;

public interface AlbumService {
	int deleteByPrimaryKey(String albumId);

    int insert(Album record);

    int insertSelective(Album record);

    Album selectByPrimaryKey(String albumId);

    int updateByPrimaryKeySelective(Album record);

    int updateByPrimaryKey(Album record);
    
    ArrayList<Album> getAlbumsByUserId(String userId);
    
    Album selectAlbumId(Album record);
}
