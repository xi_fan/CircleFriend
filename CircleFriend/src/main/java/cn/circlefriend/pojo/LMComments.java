package cn.circlefriend.pojo;

import java.util.Date;

public class LMComments {
    private String lmcommentsId;

    private String lmcommentsWhoid;

    private String lmcommentsToid;

    private String lmcommentsBelonglmid;

    private Date lmcommentsTime;

    private String lmcommentsContent;

    public String getLmcommentsId() {
        return lmcommentsId;
    }

    public void setLmcommentsId(String lmcommentsId) {
        this.lmcommentsId = lmcommentsId == null ? null : lmcommentsId.trim();
    }

    public String getLmcommentsWhoid() {
        return lmcommentsWhoid;
    }

    public void setLmcommentsWhoid(String lmcommentsWhoid) {
        this.lmcommentsWhoid = lmcommentsWhoid == null ? null : lmcommentsWhoid.trim();
    }

    public String getLmcommentsToid() {
        return lmcommentsToid;
    }

    public void setLmcommentsToid(String lmcommentsToid) {
        this.lmcommentsToid = lmcommentsToid == null ? null : lmcommentsToid.trim();
    }

    public String getLmcommentsBelonglmid() {
        return lmcommentsBelonglmid;
    }

    public void setLmcommentsBelonglmid(String lmcommentsBelonglmid) {
        this.lmcommentsBelonglmid = lmcommentsBelonglmid == null ? null : lmcommentsBelonglmid.trim();
    }

    public Date getLmcommentsTime() {
        return lmcommentsTime;
    }

    public void setLmcommentsTime(Date lmcommentsTime) {
        this.lmcommentsTime = lmcommentsTime;
    }

    public String getLmcommentsContent() {
        return lmcommentsContent;
    }

    public void setLmcommentsContent(String lmcommentsContent) {
        this.lmcommentsContent = lmcommentsContent == null ? null : lmcommentsContent.trim();
    }
}