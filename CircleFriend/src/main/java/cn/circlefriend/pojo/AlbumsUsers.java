package cn.circlefriend.pojo;

public class AlbumsUsers {
    private String albumsusersId;

    private String albumsusersAid;

    private String albumsusersUid;

    public String getAlbumsusersId() {
        return albumsusersId;
    }

    public void setAlbumsusersId(String albumsusersId) {
        this.albumsusersId = albumsusersId == null ? null : albumsusersId.trim();
    }

    public String getAlbumsusersAid() {
        return albumsusersAid;
    }

    public void setAlbumsusersAid(String albumsusersAid) {
        this.albumsusersAid = albumsusersAid == null ? null : albumsusersAid.trim();
    }

    public String getAlbumsusersUid() {
        return albumsusersUid;
    }

    public void setAlbumsusersUid(String albumsusersUid) {
        this.albumsusersUid = albumsusersUid == null ? null : albumsusersUid.trim();
    }
}