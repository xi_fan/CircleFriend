package cn.circlefriend.pojo;

import java.util.Date;

public class LeaveMessage {
    private String leavemessageId;

    private String leavemessageWhoid;

    private String leavemessageToid;

    private Date leavemessageTime;

    private String leavemessageContent;

    public String getLeavemessageId() {
        return leavemessageId;
    }

    public void setLeavemessageId(String leavemessageId) {
        this.leavemessageId = leavemessageId == null ? null : leavemessageId.trim();
    }

    public String getLeavemessageWhoid() {
        return leavemessageWhoid;
    }

    public void setLeavemessageWhoid(String leavemessageWhoid) {
        this.leavemessageWhoid = leavemessageWhoid == null ? null : leavemessageWhoid.trim();
    }

    public String getLeavemessageToid() {
        return leavemessageToid;
    }

    public void setLeavemessageToid(String leavemessageToid) {
        this.leavemessageToid = leavemessageToid == null ? null : leavemessageToid.trim();
    }

    public Date getLeavemessageTime() {
        return leavemessageTime;
    }

    public void setLeavemessageTime(Date leavemessageTime) {
        this.leavemessageTime = leavemessageTime;
    }

    public String getLeavemessageContent() {
        return leavemessageContent;
    }

    public void setLeavemessageContent(String leavemessageContent) {
        this.leavemessageContent = leavemessageContent == null ? null : leavemessageContent.trim();
    }
}