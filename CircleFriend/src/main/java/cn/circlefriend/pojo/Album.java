package cn.circlefriend.pojo;

public class Album {
    private String albumId;

    private String albumCover;
    
    private String albumName;

    private String albumPassword;

    private String albumDescription;

    private String albumCreattime;

    private String albumCreatorid;
    
    
    /**
	 * @return the albumCover
	 */
	public String getAlbumCover() {
		return albumCover;
	}

	/**
	 * @param albumCover the albumCover to set
	 */
	public void setAlbumCover(String albumCover) {
		this.albumCover = albumCover;
	}

	public String getAlbumId() {
        return albumId;
    }

    public void setAlbumId(String albumId) {
        this.albumId = albumId == null ? null : albumId.trim();
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName == null ? null : albumName.trim();
    }

    public String getAlbumPassword() {
        return albumPassword;
    }

    public void setAlbumPassword(String albumPassword) {
        this.albumPassword = albumPassword == null ? null : albumPassword.trim();
    }

    public String getAlbumDescription() {
        return albumDescription;
    }

    public void setAlbumDescription(String albumDescription) {
        this.albumDescription = albumDescription == null ? null : albumDescription.trim();
    }

    public String getAlbumCreattime() {
        return albumCreattime;
    }

    public void setAlbumCreattime(String albumCreattime) {
        this.albumCreattime = albumCreattime == null ? null : albumCreattime.trim();
    }

    public String getAlbumCreatorid() {
        return albumCreatorid;
    }

    public void setAlbumCreatorid(String albumCreatorid) {
        this.albumCreatorid = albumCreatorid == null ? null : albumCreatorid.trim();
    }
}