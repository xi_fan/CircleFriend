package cn.circlefriend.pojo;


public class AlbumComments {
    private String albumcommentsId;

    private String albumcommentsTime;

    private String albumcommentsWhoid;

    private String albumcommentsToid;

    private String albumcommentsBelongaid;

    private String albumcommentsContents;

    public String getAlbumcommentsId() {
        return albumcommentsId;
    }

    public void setAlbumcommentsId(String albumcommentsId) {
        this.albumcommentsId = albumcommentsId == null ? null : albumcommentsId.trim();
    }

    public String getAlbumcommentsTime() {
        return albumcommentsTime;
    }

    public void setAlbumcommentsTime(String albumcommentsTime) {
        this.albumcommentsTime = albumcommentsTime;
    }

    public String getAlbumcommentsWhoid() {
        return albumcommentsWhoid;
    }

    public void setAlbumcommentsWhoid(String albumcommentsWhoid) {
        this.albumcommentsWhoid = albumcommentsWhoid == null ? null : albumcommentsWhoid.trim();
    }

    public String getAlbumcommentsToid() {
        return albumcommentsToid;
    }

    public void setAlbumcommentsToid(String albumcommentsToid) {
        this.albumcommentsToid = albumcommentsToid == null ? null : albumcommentsToid.trim();
    }

    public String getAlbumcommentsBelongaid() {
        return albumcommentsBelongaid;
    }

    public void setAlbumcommentsBelongaid(String albumcommentsBelongaid) {
        this.albumcommentsBelongaid = albumcommentsBelongaid == null ? null : albumcommentsBelongaid.trim();
    }

    public String getAlbumcommentsContents() {
        return albumcommentsContents;
    }

    public void setAlbumcommentsContents(String albumcommentsContents) {
        this.albumcommentsContents = albumcommentsContents == null ? null : albumcommentsContents.trim();
    }
}