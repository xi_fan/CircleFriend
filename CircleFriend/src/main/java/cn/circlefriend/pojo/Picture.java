package cn.circlefriend.pojo;

public class Picture {
	private String pictureId;

	private String pictureAid;

	private String pictureUid;

	private String pictureUploadtime;

	private String pictureName;

	/**
	 * @return the pictureName
	 */
	public String getPictureName() {
		return pictureName;
	}

	/**
	 * @param pictureName
	 *            the pictureName to set
	 */
	public void setPictureName(String pictureName) {
		this.pictureName = pictureName;
	}

	public String getPictureId() {
		return pictureId;
	}

	public void setPictureId(String pictureId) {
		this.pictureId = pictureId == null ? null : pictureId.trim();
	}

	public String getPictureAid() {
		return pictureAid;
	}

	public void setPictureAid(String pictureAid) {
		this.pictureAid = pictureAid == null ? null : pictureAid.trim();
	}

	public String getPictureUid() {
		return pictureUid;
	}

	public void setPictureUid(String pictureUid) {
		this.pictureUid = pictureUid == null ? null : pictureUid.trim();
	}

	public String getPictureUploadtime() {
		return pictureUploadtime;
	}

	public void setPictureUploadtime(String pictureUploadtime) {
		this.pictureUploadtime = pictureUploadtime == null ? null
				: pictureUploadtime.trim();
	}
}