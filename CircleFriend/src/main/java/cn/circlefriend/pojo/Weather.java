package cn.circlefriend.pojo;

public class Weather {
    private String weatherId;

    private String weatherLocation;

    private String weatherUid;

    public String getWeatherId() {
        return weatherId;
    }

    public void setWeatherId(String weatherId) {
        this.weatherId = weatherId == null ? null : weatherId.trim();
    }

    public String getWeatherLocation() {
        return weatherLocation;
    }

    public void setWeatherLocation(String weatherLocation) {
        this.weatherLocation = weatherLocation == null ? null : weatherLocation.trim();
    }

    public String getWeatherUid() {
        return weatherUid;
    }

    public void setWeatherUid(String weatherUid) {
        this.weatherUid = weatherUid == null ? null : weatherUid.trim();
    }
}