package cn.circlefriend.pojo;

public class User {
    private String userId;

    private String userPassword;

    private String userWechatname;

    private String userPhone;

    private String userTruename;

    private String userAlbumpassword;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword == null ? null : userPassword.trim();
    }

    public String getUserWechatname() {
        return userWechatname;
    }

    public void setUserWechatname(String userWechatname) {
        this.userWechatname = userWechatname == null ? null : userWechatname.trim();
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone == null ? null : userPhone.trim();
    }

    public String getUserTruename() {
        return userTruename;
    }

    public void setUserTruename(String userTruename) {
        this.userTruename = userTruename == null ? null : userTruename.trim();
    }

    public String getUserAlbumpassword() {
        return userAlbumpassword;
    }

    public void setUserAlbumpassword(String userAlbumpassword) {
        this.userAlbumpassword = userAlbumpassword == null ? null : userAlbumpassword.trim();
    }
}